% SUDOKU
%
% Files
%   Sudoku        - TODO: 
%   SudokuDisplay - display's a solved sudoku matrix
%   SudokuGUI     - MATLAB code for SudokuGUI.fig
%   SudokuMatrix  - Builds a sudoku matrix according to given hints
%   SudokuSolve   - Given a Sudoku matrix this method solves it.
%   SudokuTest    - tests the strength of the given the hints of a Sudoku riddle
%   SudokuUpdate  - this method updates the Sudoku matrix at a given point and
