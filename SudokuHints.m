function [ Hints ] = SudokuHints(Matrix, Show)
%SUDOKUMATRIX Builds a sudoku matrix according to the known points
%   Matrix - a sudoku matrix 
%
% returns:
%   Hints - Nx3 matrix containing a vector of hints
%
% A Sudoku matrix {a}i,j,k is a matrix where a == 1 will tell us that a 
% value of k is possible in the the indices i, j on the Sudoku grid.

%% default parameters

if (~exist('Show', 'var'))
    % should not revert to this as this will take a lot of time to solve.
    Show = true;
end

%% build the hints for every place in the matrix where the possibilites are 1

Hints = ones(9, 9, 9);

matrixHints = sum(Matrix, 3) == 1;
numMatrixHints = sum(matrixHints(:));

Hints = nan(numMatrixHints, 3);

[Hints(: ,1), Hints(:, 2)] = find(matrixHints);

for ii = 1:numMatrixHints
    Hints(ii, 3) = find(Matrix(Hints(ii, 1), Hints(ii, 2),:));
end

end