function varargout = SudokuGUI(varargin)
% SUDOKUGUI MATLAB code for SudokuGUI.fig
%      SUDOKUGUI, by itself, creates a new SUDOKUGUI or raises the existing
%      singleton*.
%
%      H = SUDOKUGUI returns the handle to a new SUDOKUGUI or the handle to
%      the existing singleton*.
%
%      SUDOKUGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SUDOKUGUI.M with the given input arguments.
%
%      SUDOKUGUI('Property','Value',...) creates a new SUDOKUGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SudokuGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SudokuGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SudokuGUI

% Last Modified by GUIDE v2.5 24-Sep-2011 01:10:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SudokuGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @SudokuGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SudokuGUI is made visible.
function SudokuGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SudokuGUI (see VARARGIN)

% Choose default command line output for SudokuGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SudokuGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = SudokuGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function [] = clear_Hints()
% clear knowns from GUI
for ii = 1:9
    for jj = 1:9
        II = (ii - 1) * 9 + jj;
        obj = findobj('Tag', ['edit' num2str(II)]);
        set(obj(1), 'String', '');
    end
end
obj = findobj('Tag', 'text1');
if (~isempty(obj))
    set(obj(1), 'String', '');
end


function [] = set_Hints(Hints)
% set knowns to GUI
for iHint = 1:size(Hints, 1)
    Hint = Hints(iHint, :);
    II = (Hint(1) - 1) * 9 + Hint(2);
    obj = findobj('Tag', ['edit' num2str(II)]);
    set(obj(1), 'String', num2str(Hint(3)));
end
% this updates the matrix and the GUI but also solves it a bit, 
%SudokuMatrix(Hints, true);


function [Hints] = read_Hints()
% read knowns from GUI
Hints = [];
for ii = 1:9
    for jj = 1:9
        II = (ii - 1) * 9 + jj;
        obj = findobj('Tag', ['edit' num2str(II)]);
        str = get(obj(1), 'String');
        if (~isempty(str))
            Hints = [Hints; [ii jj str2double(str)]];
        end
    end
end


function edit_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
value = str2num(get(hObject, 'String'));
if (isempty(value))
    set(hObject, 'String', '');
elseif (any(value == 1:9))
    set(hObject, 'String', value);
else
    set(hObject, 'String', '');
end


% --- Executes during object creation, after setting all properties.
function edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% simple 1 solution exhaustive search

% build the SudokuMatrix
global Solutions;
Solutions = 0;
SudokuSolve(SudokuMatrix(read_Hints(), true), 2, true);

str = [num2str(Solutions) ' solutions found'];
display(str);
obj = findobj('Tag', 'editlog');
if (~isempty(obj))
    set(obj(1), 'String', str);
end

% SudokuTest(Hints, Show);

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% test to check which is the most critical information point (also returns
% the strenth as a bi-product) also checks the difficulty of finding the
% full solution by checking how many times it had to guess for a solution
% TODO: add counters into the UPDATE process to check real branching
% difficulties.
[HintsSolutions, Attempts, Strength] = SudokuTest(read_Hints(), true);
numHintsSolutions = numel(HintsSolutions) - 1;
numAttempts = numel(Attempts);
obj = findobj('Tag', 'editlog');
if (~isempty(obj))
    set(obj(1), 'String', ...
        char([repmat('HintSolutions', [numHintsSolutions, 1]) num2str((1:numHintsSolutions)') repmat(' : ', [numHintsSolutions, 1]) num2str(HintsSolutions(1:end - 1))], ...
             [repmat('Attempt', [numAttempts, 1]) num2str((1:numAttempts)') repmat(' : ', [numAttempts, 1]) num2str(Attempts')], ...
             ['Analyzed strength: ' Strength] ));
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear_Hints();
obj = findobj('Tag', 'popupmenu1');
[Hints] = SudokuGenerate(get(obj(1), 'String'), false);
set_Hints(Hints);


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% clear all the set grid values
clear_Hints();


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file, path] = uigetfile('*.mat', 'Choose saved file');
clear Current; % make sure we load current from the file
if (file ~= 0)
    load(fullfile(path,file));
    if (exist('Current', 'var'))
        clear_Hints();
        set_Hints(Current);
    else
        msgbox('illegal file (no matrix discription found).');
    end
end


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Current = read_Hints();
[file, path] = uiputfile('*.mat', 'Save file as');
save(fullfile(path,file), 'Current');


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function frameA_CreateFcn(hObject, eventdata, handles)
% hObject    handle to frameA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
