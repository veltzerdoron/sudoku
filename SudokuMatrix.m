function [ Matrix ] = SudokuMatrix(Hints, Show)
%SUDOKUMATRIX Builds a sudoku matrix according to the known points
%   Hints - Nx3 matrix containing a vector of hints
%
% returns:
%   Matrix - a Sudoku matrix
%
% A Sudoku matrix {a}i,j,k is a matrix where a == 1 will tell us that a 
% value of k is possible in the the indices i, j on the Sudoku grid.

%% default parameters

if (~exist('Show', 'var'))
    % should not revert to this as this will take a lot of time to solve.
    Show = true;
end

%% build the matrix by updating all the hints updating them recursively

Matrix = ones(9, 9, 9);

for iHint = 1:size(Hints, 1)
    Hint = Hints(iHint, :);
    Matrix = SudokuUpdate(Matrix, Hint(1), Hint(2), Hint(3), true, true, Show);
    if (isempty(Matrix))
        return;
    end
end
    
end

