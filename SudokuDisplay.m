function [ ] = SudokuDisplay( Matrix )
%SUDOKUDISPLAY display's a solved sudoku matrix
%   Matrix - input matrix

Possibilities = sum(Matrix, 3);
if (any(any(Possibilities == 0)))
    % this is an invalid input matrix, return
    display('invalid input matrix error - cant display');
elseif (all(all(Possibilities == 1)))
    % if this is a solution then display the solution
    [ii, jj, kk] = ind2sub(size(Matrix), find(Matrix == 1));
    Solution(sub2ind([9, 9], ii, jj)) = kk;
    display(num2str(reshape(Solution, [9, 9])));
else
    maxPossibilities = max(max(Possibilities));
    for ii = 1:9
        lineStr = repmat(' ', [1, maxPossibilities * 2 * 9]);
        firstJ = true;
        for jj = 1:9
            mm = (maxPossibilities * 2 + 1) * (jj - 1) + 1;
            if (firstJ)
                lineStr(mm)= ' ';
            else
                lineStr(mm)= ',';
            end
            mm = mm + 1;
            firstJ = false;
            firstK = true;
            for kk = 1:9
                if Matrix(ii, jj, kk)
                    if (firstK)
                        lineStr(mm)= ' ';
                    else
                        lineStr(mm)= '|';
                    end
                    mm = mm + 1;
                    firstK = false;
                    lineStr(mm) = num2str(kk);
                    mm = mm + 1;
                end
            end
        end
        display(char(lineStr));
    end
    
end

%new line
display('----------------------------------------------------------');

end

