% TODO: 
% 1 - SudokuUpdate enter globals 4 strength testing
% 2 - translate to Python
% 3 - numPy implementation


% display flag
Show = true;


% initialize matrix
% known points in the matrix
Knowns = [1, 1, 5
        1, 3, 2;
        1, 5, 4;
        1, 9, 1;

        2, 4, 9;
        2, 7, 8;

        3, 2, 6;
        3, 5, 8;
        3, 9, 5;

        4, 8, 7;

        5, 1, 8;
        5, 3, 4;
        5, 5, 1;
        5, 7, 9;
        5, 9, 3;

        6, 2, 5;

        7, 1, 2;
        7, 5, 5;
        7, 8, 4;

        8, 3, 8;
        8, 6, 6;

        9, 1, 9;
        9, 5, 3;
        9, 7, 2;
        9, 9, 8];


% simple 1 solution exhaustive search
%{
global Solutions;
Solutions = 0;
SudokuSolve(SudokuMatrix(Knowns, false), 1, Show);
if (Show)
    display([ num2str(Solutions) ' solutions found']); 
end
%}

% test to check which is the most critical information point
[KnownsSolutions, UniqueAttempts] = SudokuTest(Knowns, Show);