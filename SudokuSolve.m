function [ ] = SudokuSolve( Matrix, MaxSolutionResolution, Show)
%SUDOKUSOLVE Given a Sudoku matrix this method solves it.
%   Matrix - input (starting) state
%   MaxSolutionResolution - maximum number of solutions to find
%   Show - debug flag
%
% returns: (through globals)
%   Solutions - number of solutions found
%   Attemps - list of the attempts branching

%% default parameters

if (~exist('Matrix', 'var'))
    return;
end

if (isempty(Matrix))
    return;
end;

if (~exist('MaxSolutionResolution', 'var'))
    MaxSolutionResolution = 1;
end

if (~exist('Show', 'var'))
    Show = true;
end


%% This counts how many solutions were found and the branching attempts
global Solutions;
global Attempts;

if (Solutions == MaxSolutionResolution)
    % Enough solutions, so, return
    return;
end

% count the possibilities for each grid square.
Possibilities = sum(Matrix, 3);

if (any(any(Possibilities == 0)))
    % This is not a valid state, return
    return;
elseif (all(all(Possibilities == 1)))
    % Only one possibility for each grid square, display the matrix and exit
    if (Show)
        SudokuDisplay(Matrix);
    end
    Solutions = Solutions + 1;
    return;
else
%     SudokuDisplay(Matrix);
%     pause;

    % find the minimal possibilities grid point 
    Possibilities(Possibilities == 1) = 10; % remove ones (set indices)
    [~, I] = min(Possibilities(:));
    [ii, jj] = ind2sub(size(Possibilities), I);

    % try all posibilities starting with the place with minimal options
    Attempt = find(Matrix(ii,jj,:));
    B4Solution = Solutions;
    B4Attempts = Attempts;
    Attempts = [Attempts numel(Attempt)];
    for kk = Attempt'
        % update it and continue solving
        Updated = SudokuUpdate(Matrix, ii, jj, kk, true, true, Show);
        if (~isempty(Updated)) 
            % if this guess is seemingly valid try to continue
            SudokuSolve(Updated, MaxSolutionResolution, Show);
            if (Solutions == MaxSolutionResolution)
                % if enough solutions don't try all the other options
                % also leaves the last solution displayed
                return;
            end
        end
    end
    if (Solutions == B4Solution) 
        Attempts = B4Attempts;
    end
end
