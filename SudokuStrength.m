function [HintsSolutions, UniqueAttempts, Strength] = SudokuStrength(Hints, Show)
%SUDOKUSTRENGTH tests the strength of the given known points of a Sudoku riddle
%   Knwons - Nx3 matrix containing a vector of known grid points
%   Show - debug show flag
%
% returns:
%   HintsSolutions - Number of possible solutions for each removed known
%       point and also for the original matrix
%   Attempts - the decision tree taken during the solution, this only has
%       meaning if the solution is unique
%   Strength - analyzed strength of the image 
%              "Easy', 'Medium', 'Hard', 'VeryHard'

%% default parameters

if (~exist('Hints', 'var'))
    % should not revert to this as this will take a lot of time to solve.
    Hints = [];
end

if (~exist('Show', 'var'))
    % should not revert to this as this will take a lot of time to solve.
    Show = true;
end


%% count how many solutions exist for each removed known grid square

HintsSolutions = nan(size(Hints, 1) + 1, 1);
MaxSolutionResolution = 100;
global Solutions;

for iExclude = 1:size(Hints, 1)
    Solutions = 0;
    SudokuSolve(SudokuMatrix(Hints(1:end ~= iExclude, :), false), MaxSolutionResolution, false);
    if (Show)
        str = [num2str(Solutions) ' solutions found'];
        display(str);
%         obj = findobj('Tag', 'text1');
%         if (~isempty(obj))
%             set(obj(1), str);
%         end
    end
    
    % update the number of found solutions without this data point
    % (the more solutions found the more critical the data added by the excluded data point)
    HintsSolutions(iExclude) = Solutions;
end


%% test the strength of the regular unique solution
global Attempts;
Solutions = 0;
Attempts = [];
SudokuSolve(SudokuMatrix(Hints, false), 2, false);
HintsSolutions(end) = Solutions;
UniqueAttempts = Attempts;

% attempts should be taken notice only if the solution is unique, we ask for 2 solutions and expect to find one
if (HintsSolutions(end) ~= 1)
    % if the solution is non-unique then this is an unacceptable puzzle
    Strength = 'NA';

    if (Show)
        display('NON UNIQUE SOLUTION');
    end
else
    numHints = size(Hints, 1);
    nonWholeHintsSolutions = HintsSolutions(1:end - 1);
    maxHintsSolutions = max(nonWholeHintsSolutions);
    minHintsSolutions = min(nonWholeHintsSolutions);
    meanHintsSolutions = mean(nonWholeHintsSolutions);
    stdHintsSolutions = std(nonWholeHintsSolutions);
    
    numAttempts = numel(Attempts);
    maxAttempts = max(Attempts);
    minAttempts = min(Attempts);
    meanAttempts = mean(Attempts);
    stdAttempts = std(Attempts);

    % calculate strength heuristically analyzing different hint strengths
    % and the attempts branching
    valHintsSolutions = (meanHintsSolutions / MaxSolutionResolution + 1) / 2;
    valAttempts = numAttempts / 6;
    
    % simple averaging done here
    val = mean([valAttempts, valHints]);
    if (val < 1 / 4)
        Strength = 'Easy';
    elseif (val > 3 / 4)
        Strength = 'VeryHard';
    elseif (val < 1 / 2)
        Strength = 'Medium';
    else
        Strength = 'Hard';
    end
    

    if (Show)
%     close all;

        figure;
        plot(HintsSolutions(1:end - 1));
        title('Solution number as a function of removed known point index');
        display(['Number of knowns: ' num2str(numHints)]);
        display(['Max/Min of number of solutions: ' num2str(maxHintsSolutions) '/' num2str(minHintsSolutions)]);
        display(['Mean/Std of number of solutions: ' num2str(meanHintsSolutions) '/' num2str(stdHintsSolutions)]);

        display('-----------------------------------------------------------');

        figure;
        plot(Attempts);
        title('Number of attempts taken en route to the solution');
        display(['Number of attempted branchings: ' num2str(numAttempts)]);
        display(['Max/Min of solution attempts: ' num2str(maxAttempts) '/' num2str(minAttempts)]);
        display(['Mean of solution attempts: ' num2str(meanAttempts) '/' num2str(stdAttempts)]);

        tilefigs;
    end
end

end