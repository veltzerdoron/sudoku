function [hints] = SudokuGenerate(Strength, Show)
%SUDOKUGENERATE Generates a sudoku grid with a given strength
%   Strength - either Easy, Medium, Hard or Very hard
%   Show - debug show flag
%
% returns:
%   Hints - The resulting sudoku matrix given in puzzle hint form

%%globals
global Solutions;

%% default parameters

if (~exist('Strength', 'var'))
    % should not revert to this as this will take a lot of time to solve.
    Strength = 'Easy';
end

if (~exist('Show', 'var'))
    % should not revert to this as this will take a lot of time to solve.
    Show = true;
end

%% generate matrix
% repeat till we get a satisfactory matrix
done = false;
while (~done)
    Matrix = ones(9, 9, 9);

    % genrate first 16 hints (less than that will not be unique)
    for ii = 1:16
        Matrix = add_Random_Hint(Matrix, Show);
    end

    % add random hints to the matrix till we get a satisfactory matrix or
    % we get a different strength matrix which means we need to restart
    restart = false;
    while ((~done) || (~restart))
        % add random hint
        Matrix = add_Random_Hint(Matrix, Show);

        if (~isempty(Matrix))
            Solutions = 0;
            SudokuSolve(Matrix, 2, Show);
            if (Solutions == 1)
                [~, ~, testStrength] = SudokuStrength(SudokuHints(Matrix), Show);
                if (testStrength == Strength)
                    % required strength achieved, return
                    done = true;
                else
                    % if we get a different strength matrix then restart
                    restart = true;
                end
            elseif (Solutions == 0)
                % we reached a non solvable matrix.
                restart = true;
            end
        else
            % this hint generated a non solvable updated matrix.
            restart = true;
        end
    end

end
end

function [MatrixTag] = add_Random_Hint(Matrix, Show)
%ADD_RANDOM_HINT add a random hint to the input matrix
%   Matrix - input matrix
%
% returns:
%   MatrixTag - enlarged hint output matrix

% if the matrix is solved then no hints can be added
if (all(sum(Matrix, 3) == 1))
    MatrixTag = Matrix;
    return;
end

% randomize a hint position until we hit a non marked point
numPosses = 1;
while (numPosses < 2) 
    [hintPos] = floor(rand(2,1) * 8) + 1;
    % choose a possibility from available ones in the matrix
    Posses = Matrix(hintPos(1), hintPos(2), :);
    numPosses = sum(Posses);
end

% choose a random hint from available possibilities
hintPoss = find(cumsum(Posses) == (floor(rand(1) * (numPosses - 1)) + 1), 1, 'First');

% add the hint to the input matrix
MatrixTag = SudokuUpdate(Matrix, hintPos(1), hintPos(2), hintPoss, true, false, Show);
end