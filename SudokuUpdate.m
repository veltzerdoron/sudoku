function [ Updated ] = SudokuUpdate( Matrix, ii, jj, kk, bPos, bImPos, Show)
%SUDOKUUPDATE this method updates the Sudoku matrix at a given point and
%cascades the effect recursively
%   Matrix - current state of the matrix
%   ii/ jj/ kk - the position and value to update
%   bPos/ bImPos - flags that indicate whether to use the Possibility and/or
%       the ImPossibility logical constraints when updating the matrix
%
% returns:
%   Updated - matrix after update and cascading effect


%% default parameters

if (~exist('Show', 'var'))
    % should not revert to this as this will take a lot of time to solve.
    Show = true;
end

% if the matrix is invalid return an invalid result
if (any(any(sum(Matrix, 3) == 0)))
    Updated = [];
    return;
end


%% mark the square and the GUI if available
Updated = Matrix;
Updated(ii, jj, :) = (1:9 == kk);
if (Show)
    II = (ii - 1) * 9 + jj;
    obj = findobj('Tag', ['edit' num2str(II)]);
    if (~isempty(obj))
        set(obj(1), 'String', num2str(kk));
    end
end


%% remove possibility from row
OldPos = sum(Updated(ii, :, :), 3);
Updated(ii, 1:end ~= jj, kk) = 0;
Pos = sum(Updated(ii, :, :), 3);
% if we have new squares with 1 possibility get their index and update
if (bPos)
    for jjj = find((Pos == 1) & (OldPos > 1))
        Updated = SudokuUpdate(Updated, ii, jjj, find(Updated(ii, jjj, :) == 1), bPos, bImPos, Show);
        if (isempty(Updated))
            return;
        end
    end
end


%% remove possibility from column
OldPos = sum(Updated(:, jj, :), 3);
Updated(1:end ~= ii , jj, kk) = 0;
Pos = sum(Updated(:, jj, :), 3);
% if we have new squares with 1 possibility get their index and update
if (bPos)
    for iii = find((Pos == 1) & (OldPos > 1))'
        Updated = SudokuUpdate(Updated, iii, jj, find(Updated(iii, jj, :) == 1), bPos, bImPos, Show);
        if (isempty(Updated))
            return;
        end
    end
end


%% remove possibility from block
%calculate block boundaries
mmi = 3 * (floor((ii - 1) / 3)) + 1;
mmI = mmi + 2;
mmj = 3 * (floor((jj - 1) / 3)) + 1;
mmJ = mmj + 2;
OldPos = sum(Updated(mmi:mmI, mmj:mmJ, :), 3);
Updated(mmi:mmI, mmj:mmJ, kk) = zeros(3, 3);
Updated(ii, jj, :) = (1:9 == kk);
Pos = sum(Updated(mmi:mmI, mmj:mmJ, :), 3);
% if we have new squares with 1 possibility get their index and update
if (bPos)
    for mmm = find((Pos == 1) & (OldPos > 1))'
        [mmmi, mmmj] = ind2sub([3, 3], mmm);
        Updated = SudokuUpdate(Updated, mmi + mmmi - 1, mmj + mmmj - 1, find(Updated(mmi + mmmi - 1, mmj + mmmj - 1, :) == 1), bPos, bImPos, Show);
        if (isempty(Updated))
            return;
        end    
    end
end


%% remove impossibilities
if (bImPos)
    %% remove impossibility from row
    imPos = squeeze(sum(Updated(ii, :, :), 2));
    % whenever impos == 1 there is only one possibility for the row ii of value kk
    for kkk = find(imPos == 1)'
        % if this ii, jjj, kkk triplette is not already marked then update it
        jjj = find(Updated(ii, :, kkk) == 1);
        if (numel(jjj) > 1)
            Updated = null;
            return;
        elseif (~isempty(jjj))
            if (any(squeeze(Updated(ii, jjj, :)) ~= ((1:9)' == kkk)))
                Updated = SudokuUpdate(Updated, ii, jjj, kkk, bPos, bImPos, Show);
                if (isempty(Updated))
                    return;
                end
            end
        end
    end

    %% remove impossibility from column
    imPos = squeeze(sum(Updated(:, jj, :), 1));
    % whenever imPos == 1 we can say that the
    for kkk = find(imPos == 1)'
        % if this iii, jj, kkk triplette is not already marked then update it
        iii = find(Updated(:, jj, kkk) == 1);
        if (numel(iii) > 1)
            Updated = null;
            return;
        elseif (~isempty(iii))
            if (any(squeeze(Updated(iii, jj, :)) ~= ((1:9)' == kkk)))
                Updated = SudokuUpdate(Updated, iii, jj, kkk, bPos, bImPos, Show);
                if (isempty(Updated))
                    return;
                end
            end
        end
    end


    %% remove impossibility from block
    % block boundaries already calculated
    imPos = sum(sum(Updated(mmi:mmI, mmj:mmJ, :), 1), 2);
    for kkk = find(imPos == 1)'
        % if this mmmi, mmmj, kkk triplette is not already marked then update it
        mmm = find(Updated(mmi:mmI, mmj:mmJ, kkk) == 1);
        if (numel(mmm) > 1)
            Updated = null;
            return;
        elseif (~isempty(mmm))
            [mmmi, mmmj] = ind2sub([3, 3], mmm);
            if (any(squeeze(Updated(mmi + mmmi - 1, mmj + mmmj - 1, :)) ~= ((1:9)' == kkk)))
                Updated = SudokuUpdate(Updated, mmi + mmmi - 1, mmj + mmmj - 1, kkk, bPos, bImPos, Show);
                if (isempty(Updated))
                    return;
                end
            end
        end
    end
end

end